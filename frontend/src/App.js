import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import IssueList from './components/IssueList';
import IssueDetail from './components/IssueDetail';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={IssueList} />
        <Route path="/issue/:number" component={IssueDetail} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
