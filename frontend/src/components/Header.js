import React from 'react'

const Header = () => (
  <center>
    <div className="hero hero-sm bg-dark">
      <div className="hero-body">
        <h1>Repository Issue List</h1>
        <p>facebook/react</p>
      </div>
    </div>
  </center>
)

export default Header;