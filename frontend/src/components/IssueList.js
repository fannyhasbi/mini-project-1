import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

import Header from './Header';

import { BACKEND_URL } from '../constants/const';

class IssueList extends React.Component {
  constructor(props){
    super(props);
    
    this.state = {
      isLoading: true,
      repository: {
        issues: {
          nodes: []
        }
      }
    }
  }

  componentDidMount(){
    axios({
      method: "POST",
      url: BACKEND_URL + "/issues",
      data: {}
    })
    .then(response => {
      this.setState({
        isLoading: false,
        repository: response.data.data.repository
      });
    });
  }

  render(){
    var content;

    if(this.state.isLoading){
      content = <div className="loading loading-lg"></div>
    }
    else {
      content = (
        <table className="table">
          <thead>
            <tr>
              <th>#Number</th>
              <th className="text-center">Title</th>
            </tr>
          </thead>
          <tbody>
            {this.state.repository.issues.nodes.map((item, i) => (
              <tr key={"issue-"+i}>
                <td><Link to={"/issue/"+item.number}><b>#{item.number}</b></Link></td>
                <td>{item.title}</td>
              </tr>
            ))}
          </tbody>
        </table>
      )
    }

    return (
      <div>
        <Header />
        <div className="container">
          <div className="columns">
            <div className="column col-8 col-mx-auto">
              { content }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default IssueList;