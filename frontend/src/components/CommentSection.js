import React from 'react';

const CommentSection = (props) => {
  const { comm } = props;
  var result;
  
  if(comm.length === 0){
    result = <p>No comments</p>
  }
  else {
    result = comm.map((item, i) => (
      <div className="tile" key={"comment-"+i} style={{marginTop: "50px"}}>
        <div className="tile-icon">
          <figure className="avatar avatar-lg">
            <img src={item.author.avatarUrl} alt="img" />
          </figure>
        </div>
        <div className="tile-content bg-gray" style={{paddingLeft: "5px", paddingRight: "5px"}}>
          <h5 className="tile-title"><a href={item.author.url} target="__blank">{item.author.login}</a></h5>
          <p
            className="tile-subtitle"
            dangerouslySetInnerHTML={{__html: item.bodyHTML}}>
          </p>
        </div>
      </div>
    ))
  }

  return result;
}

export default CommentSection;