import React from 'react';
import axios from 'axios';

import CommentSection from './CommentSection';

import { BACKEND_URL } from '../constants/const';

class IssueDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      issue: {}
    }
  }

  componentDidMount() {
    const num = this.props.match.params.number;
    const formData = new FormData();
    formData.append('number', num)

    axios({
      method: "POST",
      url: BACKEND_URL + "/issue-detail",
      headers: {
        'content-type': 'multipart/form-data'
      },
      data: formData
    })
      .then(response => {
        console.log(response);
        this.setState({
          isLoading: false,
          issue: response.data.data.repository.issue
        });
      });
  }

  render() {
    const { issue, isLoading } = this.state;
    var content;

    if(isLoading){
      content = <div className="loading loading-lg"></div>
    }
    else {
      content = (
        <div className="container">
          <div className="col-10 col-mx-auto">
            <h3><b>#{issue.number}</b> - {issue.title}</h3>
            <p><b>author:</b> <a href={issue.author.url} target="__blank">{issue.author.login}</a></p>

            <p dangerouslySetInnerHTML={{__html: issue.bodyHTML}}></p>
          </div>
          
          <div className="container">
            <div className="columns">
              <div className="column col-8 col-mx-auto">
                <h5>Comments</h5>
                <CommentSection comm={issue.comments.nodes} />
              </div>
            </div>
          </div>
        </div>
      );
    }

    return (
      <div>
        { content }
      </div>
    )
  }
}

export default IssueDetail;