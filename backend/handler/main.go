package handler

import (
	"net/http"

	"github.com/labstack/echo"
)

func MainHandler(c echo.Context) error {
	return c.String(http.StatusOK, "Hello world")
}
