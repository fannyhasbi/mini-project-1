package handler

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/labstack/echo"
	"gitlab.com/fannyhasbi/mini-project-1/backend/constants"
)

func IssuesHandler(c echo.Context) error {
	requestBody, err := json.Marshal(map[string]string{
		"query": `query {
			repository(owner: "facebook", name: "react"){
				issues(last: 20){
					nodes {
						number
						title
					}
				}
			}
		}
		`,
	})

	if err != nil {
		log.Fatalln(err)
	}

	// create http client
	timeout := time.Duration(constants.TIMEOUT * time.Second)
	client := http.Client{
		Timeout: timeout,
	}

	request, err := http.NewRequest("POST", "https://api.github.com/graphql", bytes.NewBuffer(requestBody))
	request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", os.Getenv("GITHUB_TOKEN")))
	request.Header.Set("Content-Type", "application/json")

	if err != nil {
		log.Fatalln(err)
	}

	resp, err := client.Do(request)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	var result interface{}

	err = json.Unmarshal(body, &result)
	if err != nil {
		log.Fatalln(err)
	}

	return c.JSON(http.StatusOK, result)
}
