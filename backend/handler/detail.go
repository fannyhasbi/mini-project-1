package handler

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/labstack/echo"
	"gitlab.com/fannyhasbi/mini-project-1/backend/constants"
)

const (
	TIMEOUT = 5
)

func IssueDetailHandler(c echo.Context) error {
	num := c.FormValue("number")
	number, err := strconv.ParseInt(num, 10, 64)
	if err != nil {
		log.Fatalln(err)
	}

	requestBody, err := json.Marshal(map[string]string{
		"query": fmt.Sprintf(`query {
			repository(owner: "facebook", name: "react"){
				issue(number: %d){
					number
					title
					bodyHTML
					author {
						login
						url
					}
					comments(last: 10) {
						nodes {
							author {
								avatarUrl(size: 64)
								login
								url
							}
							bodyHTML
						}
					}
				}
			}
		}
		`, number),
	})

	if err != nil {
		log.Fatalln(err)
	}

	// create http client
	timeout := time.Duration(constants.TIMEOUT * time.Second)
	client := http.Client{
		Timeout: timeout,
	}

	request, err := http.NewRequest("POST", "https://api.github.com/graphql", bytes.NewBuffer(requestBody))
	request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", os.Getenv("GITHUB_TOKEN")))
	request.Header.Set("Content-Type", "application/json")

	if err != nil {
		log.Fatalln(err)
	}

	resp, err := client.Do(request)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	var result interface{}

	err = json.Unmarshal(body, &result)
	if err != nil {
		log.Fatalln(err)
	}

	return c.JSON(http.StatusOK, result)
}
