package main

import (
	"fmt"
	"log"

	"github.com/joho/godotenv"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	"gitlab.com/fannyhasbi/mini-project-1/backend/handler"
)

const (
	PORT = 8080
)

// Init configurations
func init() {
	// show line number when logging
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	// loading environment variables
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

func main() {
	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
	}))

	e.GET("/", handler.MainHandler)
	e.POST("/issues", handler.IssuesHandler)
	e.POST("/issue-detail", handler.IssueDetailHandler)

	e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", PORT)))
}
